Aleph labs ember2react training + how to graphql
===

**Submitted by** thong@aleph-labs

**Period** 08/02/2021 → 15/02/2021

Target
---

- [x] familiar with git, git diff, bitbucket
- [x] understand react
- [ ] understand how to graphql
- [ ] unit testing 
- [ ] optional: code review

Daily training
---

### Monday 08th Feb

#### Overview S2B (Straight To Banks) project about development, deployment flow:
- Basic understand which modules (ID Reg, Bill Pay, Tax Pay, RFQ) team are 
working in this time. 
- Basic understand development flow, ci/cd with 4 envs CIT (QA and GOLD), SIT 
(QA and GOLD)
- dev -> PR -> Internal Review -> External Review -> CI/CD's client -> Release
on SIT env -> test

#### Understand git, git diff, bitbucket
- refer [this link](git_diff), was recommend by @lephuc and @dhruval
- use git + bitbucket as source control

#### Understand how to graphql

###### 1. Fundamental

  - GraphQL was developed and open-sourced by Facebook 

  - GraphQL is a query language for APIs - not databases

  - GraphQL is not only for React Developers

###### 2. GraphQL is the better REST

  - I don't think this is good opinions to explore, compare cons/pros without 
  considers domains or use case of real world requirements are mess. So this part
  I will focus on what's pros/cons of GraphQL

  - Pros:

    - support client side power way about fetching data

    - network performance improvements

    - loose coupling between client and server

    - strong typing

  - Cons:

    - kiss HTTP caching goodbye, everything is POST

    - before GraphQL, [FQG](fql) was Facebook's tech that did not go so well

    - strongly need engineering discipline care about GraphQL servers become big 
    and bloated

###### 3. Core concept

  - The Schema Definition Language (SDL)

    ```typescript

      type Query {
        allPersons(last: Int): [Person!]!
      }

      type Mutation {
        createPerson(name: String!, age: Int!): Person!
      }

      type Subscription {
        newPerson: Person!
      }

      type Person {
        name: String!
        age: Int!
        posts: [Post!]!
      }

      type Post {
        title: String!
        author: Person!
      }

    ```

  - Fetching data with `Queries`

    ```typescript

      // basic
      {
        allPersons {
          name
        }
      }

      // with arguments
      {
        allPersons(last: 2) {
          name
        }
      }

    ```

  - Writing data with `Mutation` (CUD)

    ```typescript

      mutation {
        createPerson(name: "Bob", age: 36) {
          name
          age
        }
      }

    ```

  - Realtime updates with Subscriptions

    ```typescript

      subscription {
        newPerson {
          name
          age
        }
      }

    ```

###### 4. Architecture

  - GraphQL server with a connected database

  ```mermaid

    graph TD;
      fe_mobile-->graphql;
      fe_web-->graphql;
      graphql-->db;

  ```

  - GraphQL layer that integrates existing systems

  ```mermaid

    graph TD;
      fe_mobile-->graphql;
      fe_web-->graphql;
      graphql-->microservices;
      microservices-->graphql;
      graphql-->legalcy_system;
      legalcy_system-->graphql;
      graphql-->3rd_api;
      3rd_api-->graphql;

  ```

  - Hybrid approach with connected database and integration of existing system

  ```mermaid

    graph TD;
      fe_mobile-->graphql;
      fe_web-->graphql;
      graphql-->legalcy_system;
      legalcy_system-->graphql;
      graphql-->3rd_api;
      3rd_api-->graphql;

  ```

  - Resolver Functions: the sole purpose of a resolver function is to fetch the
    data for its field.

  - GraphQL Client Libraries

  ```bash

  # describe data requirements

  # display data in UI
  
  ```

###### 4. Client React+Apollo tutorial (40%)

### Tuesday 09th Feb

#### Understand how to graphql

###### 4. Client React+Apollo tutorial (100%) :white_check_mark:

###### 5. Optional: Backend graphql-go Tutorial :white_check_mark:

[git_diff]: https://medium.com/@kurtisnusbaum/stacked-diffs-keeping-phabricator-diffs-small-d9964f4dcfa6#:~:text=Here%27s%20the%20basic%20strategy%3A,Phabricator%20Diffs%20for%20each%20commit.
[fql]: https://en.wikipedia.org/wiki/Facebook_Query_Language