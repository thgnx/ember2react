import { gql, useMutation } from '@apollo/client'
import React, { useState } from 'react'
import { useHistory } from 'react-router';
import { LINKS_PER_PAGE } from '../constants';
import { FEED_QUERY } from './LinkList';

const CREATE_LINK_MUTATION = gql`
  mutation PostMutation(
    $description: String!
    $url: String!
  ) {
    post(description: $description, url: $url) {
      id
      createdAt
      url
      description
    }
  }
`

const CreateLink = () => {
  const history = useHistory();

  const [formValue, setFormValue] = useState({
    url: '',
    description: ''
  })

  const [createLink] = useMutation(CREATE_LINK_MUTATION, {
    variables: {
      url:         formValue && formValue.url,
      description: formValue && formValue.description
    },
    update: (cache, { data: { post } }) => {
      const take = LINKS_PER_PAGE;
      const skip = 0;
      const orderBy = { createdAt: 'desc' };

      const data = cache.readQuery({
        query: FEED_QUERY,
        variables: {
          take,
          skip,
          orderBy
        }
      });

      cache.writeQuery({
        query: FEED_QUERY,
        data: {
          feed: {
            links: [post, ...data.feed.links]
          }
        },
        variables: {
          take,
          skip,
          orderBy
        }
      });
    },
    onCompleted: () => history.push('/new/1')
  })

  return (
    <div>
      <form onSubmit={e => {
        e.preventDefault();
        createLink();
      }}>
        <div className="flex flex-column mt3">
          <input 
            className="mb2"
            value={formValue.description}
            onChange={e => setFormValue({
              description: e.target.value,
              url: formValue.url
            })}
            type="text"
            placeholder="Type the description"/>
          <input 
            className="mb2"
            value={formValue.url}
            onChange={e => setFormValue({
              url: e.target.value,
              description: formValue.description
            })}
            type="text"
            placeholder="Type the description"/>

        </div>
        <button type="submit">Create New Links</button>
      </form>
    </div>
  )
}

export default CreateLink
